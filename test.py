import tensorflow as tf
from tensorflow import keras

# tool imports
import numpy as np
import pandas as pd

from sklearn import metrics

# matplot imports
import matplotlib
import matplotlib.pyplot as plt

# tensorflow
print ("test tf", tf.reduce_sum(tf.random.normal([1000, 1000])))

# numpy
tarr = np.array([1,2,3,4])
print ("test np", tarr)

# pandas
d = {'col1': [5, 7], 'col2': [6, 8]}
df = pd.DataFrame(data=d)
print ("test pandas", df)

# sklearn
y = np.array([1, 1, 2, 2])
pred = np.array([0.1, 0.4, 0.35, 0.8])

fpr, tpr, thresholds = metrics.roc_curve(y, pred, pos_label=2)
print ("test sklearn", metrics.auc(fpr, tpr)) # should be 0.75

# matplotlib
print ("test matplotlib")
plt.plot([1, 2, 3, 4])
plt.ylabel('some numbers')
plt.show()

