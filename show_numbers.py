import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from train_digits import read_settings, preproc

# load from yaml
settings = read_settings()

# Read in data, convert to numpy array
numbers_df = pd.read_csv(settings["train_data_file"])
numbers = numbers_df.to_numpy()

n_entries = numbers.shape[0]
print("Loaded", n_entries, "images")

# rescales to 0-1 range and reshapes to 28x28
data,labels = preproc(numbers, False)

split_test = settings["split_test"]
split_index = int(round(split_test*len(numbers)))

data_test = data[:split_index]
labels_test = labels[:split_index]
data_train = data[split_index:]
labels_train = labels[split_index:]

print ("Check shapes:")
print ("data test", data_test.shape)
print ("data train", data_train.shape)
print ("labels test", labels_test.shape)
print ("labels train", labels_train.shape)

# to get correct indices need to train without shuffling
# training code outputs
# can modify this to save/read arrays at some point
image_indices = [  43,   89,  125,  191,  240,  467,  528,  622,  644,  679,  799,
        856,  868,  897, 1050, 1107, 1147, 1148, 1220, 1224, 1288, 1299,
       1421, 1484, 1772, 1774, 1823, 1884, 1935, 1948, 2143, 2158, 2220,
       2284, 2316, 2453, 2541, 2594, 2628, 2874, 2892, 2915, 2921, 2940,
       2981, 3065, 3095, 3114, 3136, 3232, 3278, 3476, 3504, 3543, 3683,
       3761, 3846, 3950, 3960, 3997, 4018, 4047, 4169]

image_preds = [9, 4, 8, 4, 2, 9, 5, 2, 7, 8, 8, 9, 9, 8, 5, 5, 1, 5, 1, 8, 5, 0, 4, 9, 2, 9, 5, 5, 9, 7, 2, 8, 0, 4, 7, 6, 4, 5, 2, 8, 2, 2, 8, 9, 8, 2, 5, 5, 9, 9, 8, 5, 8, 1, 5, 1, 9, 4, 7, 4, 5, 9, 6]

# probably want to look at why test are not ID correctly
iin = 0
for im in image_indices:
    print("True label =", labels_test[im], "Prediction =", image_preds[iin])
    iin+=1
    plt.imshow(data_test[im], cmap='Greys')
    plt.show()
     
