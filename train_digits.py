# Image recognition for numbers in MNIST dataset for Kaggle

import os

# tool imports
import tensorflow as tf
from tensorflow import keras
from tensorflow.python.keras.callbacks import TensorBoard, EarlyStopping

from tensorboard import program

import numpy as np
import pandas as pd
import yaml

#from sklearn import metrics
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import accuracy_score

# matplot imports
import matplotlib
import matplotlib.pyplot as plt

# others
import datetime

def read_settings():
    with open(r'C:\Users\Adam\Documents\Projects\digits\settings.yaml') as file:
        settings_list = yaml.load(file, Loader=yaml.FullLoader)

    print("Loaded settings from yaml file:", settings_list)
    return settings_list

def preproc(data_raw, for_sub):

    # split off labels column
    labels=[]
    # for sub is option to specify if this is training data or not
    # final test data does not come with any labels
    if not (for_sub):
        labels = data_raw[:,0]
        data = np.delete(data_raw, [0], axis=1)
    else:
        data = data_raw

    # scale the data
    # assuming min and max known and will be the same in any sample
    scaler=MinMaxScaler(feature_range=(0,1))
    data = scaler.fit_transform(data)
    # go from 1D array of pixels to 2D, encodes x-y dependence
    data = data.reshape((data.shape[0],28,28))
    return data,labels

def create_model(pix_x, pix_y, num_classes):
    ishape = (pix_x,pix_y,1)

    # create sequential model
    model = tf.keras.models.Sequential()
    # add layers
    model.add(keras.layers.Conv2D(64, kernel_size=5, activation='relu', input_shape=ishape))
    model.add(keras.layers.ReLU(max_value=1.0))
    model.add(keras.layers.Conv2D(32, kernel_size=5, activation='relu'))
    model.add(keras.layers.ReLU(max_value=1.0))
    model.add(keras.layers.Flatten())
    model.add(keras.layers.Dense(num_classes, activation='softmax'))
    
    model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=5e-4),
                  loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                  metrics=['accuracy'])
    return model

def main():

    # load from yaml
    settings = read_settings()

    # Start tensorboard in same program
    tb = program.TensorBoard()
    tb.configure(argv=[None, "--logdir", "logs"])
    url = tb.launch()
    print(f"Tensorflow listening on {url}")

    # File for storing optimal epochs
    fname_epochs = "epochs_for_final.txt"
    n_max_epochs = 100

    # Read in data, convert to numpy array
    numbers_df = pd.read_csv(settings["train_data_file"])
    numbers = numbers_df.to_numpy()

    # for testing shorten data
    # numbers = numbers[:100]
    # np.random.shuffle(numbers)

    n_entries = numbers.shape[0]
    print("Loaded", n_entries, "images")

    # rescales to 0-1 range and reshapes to 28x28
    data,labels = preproc(numbers, False)

    # test/train splitting
    split_test = settings["split_test"]
    # for final training to use on test data, want to use all training
    # if not final part of train data is taken for validation
    if (settings["train_final"]):
        split_test = 0.
        if not os.path.isfile(fname_epochs):
            print("No file " + fname_epochs + ", run on test/train split first to determine optimal number of training epochs!")
            exit()
        else:
            ep_in = open(fname_epochs,"r")
            n_max_epochs = int(ep_in.read())
            ep_in.close()

    split_index = int(round(split_test*len(numbers)))

    data_test = data[:split_index]
    labels_test = labels[:split_index]
    data_train = data[split_index:]
    labels_train = labels[split_index:]

    print ("Check shapes:")
    print ("data test", data_test.shape)
    print ("data train", data_train.shape)
    print ("labels test", labels_test.shape)
    print ("labels train", labels_train.shape)

    # TensorBoard callback
    log_dir = "logs\\" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    print(log_dir)
    tensorboard_call = TensorBoard(log_dir=log_dir, histogram_freq=1)

    # EarlyStopping callback, stop when validation has not increased for 10 epochs
    es = EarlyStopping(monitor="accuracy", min_delta=0.001, mode="auto", verbose=1, patience=settings["epochs"])

    # create and train the model
    mod = create_model(settings["pix_x"], settings["pix_y"], settings["n_class"])

    if not (settings["train_final"]):
        mod.fit(data_train,labels_train,validation_data=(data_test,labels_test), epochs=n_max_epochs, callbacks=[tensorboard_call, es])

        # save to text file for next run
        optimal_epochs = es.stopped_epoch - int(settings["epochs"])
        print("Optimal epochs for training:", optimal_epochs)

        fout_epochs = open(fname_epochs, "w")
        fout_epochs.write("%s" % optimal_epochs)
        fout_epochs.close()

        # Apply to test dataset
        test_probs=mod.predict(data_test)

        # test_maxprob=np.amax(test_probs,axis=1)
        # print("Max probability", test_maxprob)

        # argmax with axis 1 returns column index of max value in each row
        # ie the category that was predicted to be most likely
        test_preds=np.argmax(test_probs,axis=1)

        print("Accuracy on test split:")
        print(accuracy_score(labels_test, test_preds, normalize=True))

        print("Predictions:", test_preds)
        print("True values:", labels_test)

        # for debugging, get indices of test events that got wrong predictions
        wrongindices =  np.where(test_preds != labels_test)
        print("Test events with wrong predictions:", wrongindices)

        wronglabels = test_preds[wrongindices]
        # tolist to get correct format to copy/paste
        print("With predictions:", wronglabels.tolist())

    elif(settings["train_final"]):
        
        # final fit uses optimal epochs from previous training, no early stopping callback or validation data
        print("\n\n\nFinal training for", n_max_epochs, "epochs")
        mod.fit(data_train,labels_train, epochs=n_max_epochs, callbacks=[tensorboard_call])
        
        # Read in test data, convert to numpy array
        numbers_df = pd.read_csv(settings["test_data_file"])
        numbers = numbers_df.to_numpy()

        # use same preprocessing, but labels will come back empty
        final_data_test, nolabels = preproc(numbers, True)

        # do predictions
        final_probs = mod.predict(final_data_test)
        final_preds = np.argmax(final_probs,axis=1)

        # quick sanity check, should have roughly 10% of each class
        print ("Class, Fraction of total predicted:")
        num_test = len(final_preds)
        for ii in range(0, settings["n_class"]):
            print ("{:d}, {:.3f}".format(ii, np.count_nonzero(final_preds == ii)/num_test))

        # write to text file in correct format
        out_data = pd.DataFrame(final_preds)
        # manually write index starting from 1 rather than 0
        out_data.index = np.arange(1,(len(out_data)+1))
        out_data.to_csv("out.txt", header=["Label"], index=True, index_label="ImageId", sep=",")

    else:
        print("train_final not set correctly? Check yaml file.")

    exit()

if __name__ == "__main__":
    main()
